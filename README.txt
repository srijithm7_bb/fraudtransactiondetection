===========
Fraud Transaction Detection
===========

Fraud Transaction Detection provides a model developed by python sklearn library clustering and classification algorithm for finding the fraud transactions in a given dataset. You might find
it most useful for tasks involving finding which transactions are fraudulent transactions in a given dataset. It supports both CSV and ods file types as of now and a single sample record can be provided as an array.

Installing the latest release::

    pip3 install fraudtransaction-task

Typical usage often looks like this::

    #!/usr/bin/env python

    import frauddetection.use_model as fd

    fd.FraudDetectionPredict.predictSingleSample([ 0, 1, 1, 1, 1, 0, 188, 174, 0, 1, 3, 3, 8, 52, 1, 1, 1, 1])
    fd.FraudDetectionPredict.predictDatasetCsv('data.csv') #path to csv file as argument
    fd.FraudDetectionPredict.predictDatasetOds('data.ods') #path to ods file as argument

Note
-------------

When providing a single sample, the feature values should be provided as an array excluding the consumer id and gender column value.

Output
=========

Output look like this::

    [1]
    [0 0 1 ... 0 0 0]
    [0 0 0 ... 0 1 0]

* 0 denotes normal transaction

* 1 denotes anomaly transaction (fraud transaction)
