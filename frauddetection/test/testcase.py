import unittest
import frauddetection.use_model as fu
import frauddetection.use_sdk as fsdk
import frauddetection.build_model as fb


class TestSum(unittest.TestCase):

    def test_singlesample(self):
        self.assertEqual(fu.FraudDetectionPredict.predictSingleSample([ 0, 1, 1, 1, 1, 0, 188, 174, 0, 1, 3, 3, 8, 52, 1, 1, 1, 1]) , None)

    def test_csv(self):
        self.assertEqual(fu.FraudDetectionPredict.predictDatasetCsv('data.csv') , None)

    def test_ods(self):
        self.assertEqual(fu.FraudDetectionPredict.predictDatasetOds('data.ods') , None)

    def test_use_sdk(self):
        self.assertEqual(fsdk.FraudDetectionUse.use(0) , None)

    def test_build_model(self):
        self.assertEqual(fb.FraudDetectionModel.elbowMethod(0) , None)
        self.assertEqual(fb.FraudDetectionModel.clusteringPlot(0), None)
        self.assertEqual(fb.FraudDetectionModel.clusteringDendogram(0), None)
        self.assertEqual(fb.FraudDetectionModel.buildModel(0), None)


if __name__ == '__main__':
    unittest.main()