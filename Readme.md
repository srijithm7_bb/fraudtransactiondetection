

## GOAL

Perform clustering and subsequently classification to visualize the consumer group and classify consumers that are bad.



## Data Set Analysis

- Marketing platform data
- 20 columns
- 10,000 rows
- Size of the data : 23.2 MB 



## Data Pre-processing 

- Handling Missing Values
- Check for duplicate rows - No duplicates found
- Check for unnecessary columns
- Check for Redundant information – removed gender column as has_gender column provided the same information

## Elbow method

- WCSS provide information on compactness of clusters.
- Elbow method suggested 5 different possible clusters. Chose 5 clusters as adding another cluster doesn’t improve much better the total WCSS.

## Hierachical Clustering

- Clusters 2, 4 and 5 have more distribution suggesting that the transaction behaviour is normal
- Clusters 1 and 3 are the anomalies with data that are not normal as expected.
- Created a new dataframe adding a cluster column which denotes the cluster of transaction
- Added another column to be used as label in classification which is replaced by 0 for clusters 2,3 & 4 and 1 for clusters 1 and 3 which are considered fraudulent transactions. 

![img](https://bitbucket.org/srijithm7_bb/fraudtransactiondetection/raw/0388d09d6eba708855670f3cd7c9c2350e93a532/images/hierarchical_Clustering.png)

## Classification – Logistic Regression

- Logistic regression is a powerful statistical way of modelling a binomial outcome.

- From clustering, the label to be classified is determined as 1 or 0 (whether the transaction is anomaly or not)

- Data split – 75% train data and 25% test data

- Confusion matrix

   [2477 0]

   [0    23]

- Classification accuracy 

                    precision    recall  f1-score   support
    
        fraud_No       1.00      1.00      1.00      2477
       fraud_Yes       1.00      1.00      1.00        23
       
        accuracy                           1.00      2500
       macro avg       1.00      1.00      1.00      2500
    weighted avg       1.00      1.00      1.00      2500


## Analysis

- Classification using logistic regression provided 100% accuracy in test data
- The reason is because the clusters 1 and 3 from clustering have distinct features that have been easily recognised by the algorithm.
- Cluster 1 is partitioned based on avg_claims > 1
- Cluster 3 is partitioned based on improper personal information like missing first name and last name of a customer
- These improper information is identified in Hierarchical clustering which separated the anomaly data cluster far from other clusters



## Full functioning SDK of the model

- Classification model is stored as pkl file and used in use_model.py for predicting new data
- supports both CSV and ODS file types and a single record can be provided as an array argument

### Installing the latest release::

    pip3 install fraudtransaction-task

- Usage:

    import frauddetection.use_model as fd
    
    fd.FraudDetectionPredict.predictSingleSample([ 0, 1, 1, 1, 1, 0, 188, 174, 0, 1, 3, 3, 8, 52, 1, 1, 1, 1])
    fd.FraudDetectionPredict.predictDatasetCsv('data.csv')
    fd.FraudDetectionPredict.predictDatasetOds('data.ods')

